def is_number(number):
    if isinstance(number, int) or isinstance(number, float):
        return True
    else:
        return False


def adjust_negative_numbers(num1, num2):
    if not is_number(num1) or not is_number(num2):
        raise ValueError('Inputs must be numeric')

    if num1 < 0 and num2 < 0:
        num1 = abs(num1)
        num2 = abs(num2)
    elif num1 < 0 or num2 < 0:
        num1 = -abs(num1)
        num2 = abs(num2)
    return num1, num2


def add_numbers(num1, num2):
    if not is_number(num1) or not is_number(num2):
        raise ValueError('Inputs must be numeric')
    return num1 + num2


def subtract_numbers(num1, num2):
    return add_numbers(num1, -num2)


def multiply_numbers(num1, num2):
    result = 0

    num1, num2 = adjust_negative_numbers(num1, num2)

    if not isinstance(num2, int):
        raise TypeError('Cannot multiply by a non-integer number')

    for i in range(num2):
        result = add_numbers(result, num1)
    return result
